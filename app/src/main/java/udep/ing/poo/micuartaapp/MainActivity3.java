package udep.ing.poo.micuartaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import udep.ing.poo.micuartaapp.db.Conexion;
import udep.ing.poo.micuartaapp.db.Usuario;

public class MainActivity3 extends AppCompatActivity implements View.OnClickListener {

    EditText editId, editNombre, editTelefono;
    Button botonEditar, botonEliminar;
    Conexion conexion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        this.conexion = new Conexion(this);

        editId = (EditText)findViewById(R.id.editId);
        editNombre = (EditText)findViewById(R.id.editNombre);
        editTelefono = (EditText)findViewById(R.id.editTelefono);

        Bundle extras = getIntent().getExtras();
        int id = extras.getInt(Usuario.CAMPO_ID);
        String nombre = extras.getString(Usuario.CAMPO_NOMBRE);
        String telefono = extras.getString(Usuario.CAMPO_TELEFONO);

        editId.setText(id+"");
        editId.setEnabled(false);

        editNombre.setText(nombre);
        editTelefono.setText(telefono);

        botonEditar = (Button)findViewById(R.id.button4);
        botonEliminar = (Button)findViewById(R.id.button5);

        botonEditar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        SQLiteDatabase db = conexion.getWritableDatabase();
        String[] idUsuario = {editId.getText().toString()};

        if (view.getId() == botonEditar.getId()) {

            ContentValues values = new ContentValues();
            values.put(Usuario.CAMPO_NOMBRE, editNombre.getText().toString());
            values.put(Usuario.CAMPO_TELEFONO, editTelefono.getText().toString());

            db.update(Usuario.TABLA_USUARIOS, values, Usuario.CAMPO_ID+"=?", idUsuario);
            Toast.makeText(this, "Se actualizó el usuario correctamente", Toast.LENGTH_LONG).show();

            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);

        }

        if (view.getId() == botonEliminar.getId()) {

            db.delete(Usuario.TABLA_USUARIOS, Usuario.CAMPO_ID+"=?", idUsuario);
            Toast.makeText(this, "Se eliminó el usuario correctamente", Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);

        }

    }
}