package udep.ing.poo.micuartaapp.db;

/* Usuario es una clase entidad
 * representa una tabla de la base de datos
 */
public class Usuario {

    private int id;
    private String nombre;
    private String telefono;

    public Usuario(int id, String nombre, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public static final String CREAR_TABLA_USUARIO = "CREATE TABLE usuarios (id INTEGER, nombre TEXT, telefono TEXT) ";
    public static final String DROPEAR_TABLA_USUARIO = "DROP TABLE IF EXISTS usuarios ";

    public static final String TABLA_USUARIOS = "usuarios";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_NOMBRE = "nombre";
    public static final String CAMPO_TELEFONO = "telefono";

}
