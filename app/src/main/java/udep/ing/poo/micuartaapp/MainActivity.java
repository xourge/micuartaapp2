package udep.ing.poo.micuartaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.LinkedList;
import java.util.List;

import udep.ing.poo.micuartaapp.db.Conexion;
import udep.ing.poo.micuartaapp.db.Usuario;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    Button button;
    ListView listView1;

    private Conexion conexion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.conexion = new Conexion(this);

        button = (Button)findViewById(R.id.button);
        button.setOnClickListener(this);

        listView1 = (ListView)findViewById(R.id.listView1);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getLista());
        listView1.setAdapter(adaptador);
        listView1.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == button.getId()) {

            Intent i = new Intent(this, MainActivity2.class);
            startActivity(i);

        }

    }

    private List<Usuario> listaUsuarios;

    private List<String> getLista() {

        listaUsuarios = new LinkedList<Usuario>();
        List<String> listaNombres = new LinkedList<String>();

        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Usuario.CAMPO_ID, Usuario.CAMPO_NOMBRE, Usuario.CAMPO_TELEFONO};

        Cursor cursor = db.query(Usuario.TABLA_USUARIOS, campos, null, null, null, null, null);
        while (cursor.moveToNext()) {

            int id = cursor.getInt(0);
            String nombre = cursor.getString(1);
            String telefono = cursor.getString(2);

            Usuario u = new Usuario(id, nombre, telefono);
            listaUsuarios.add(u);

            listaNombres.add(u.getNombre());

        }

        return listaNombres;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Usuario u = listaUsuarios.get(i);
        Intent intent = new Intent(this, MainActivity3.class);

        Bundle extras = new Bundle();
        extras.putInt(Usuario.CAMPO_ID, u.getId());
        extras.putString(Usuario.CAMPO_NOMBRE, u.getNombre());
        extras.putString(Usuario.CAMPO_TELEFONO, u.getTelefono());
        intent.putExtras(extras);

        startActivity(intent);

    }
}