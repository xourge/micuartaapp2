package udep.ing.poo.micuartaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import udep.ing.poo.micuartaapp.db.Conexion;
import udep.ing.poo.micuartaapp.db.Usuario;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {

    Button button2;
    EditText insertId, insertNombre, insertTelefono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(this);

        insertId = (EditText) findViewById(R.id.insertId);
        insertNombre = (EditText) findViewById(R.id.insertNombre);
        insertTelefono = (EditText) findViewById(R.id.insertTelefono);

    }

    @Override
    public void onClick(View view) {

        if (view.getId() == button2.getId()) {

            String id = insertId.getText().toString();
            String nombre = insertNombre.getText().toString();
            String telefono = insertTelefono.getText().toString();

            if (!id.equals("") && !nombre.equals("")) {

                Conexion conexion = new Conexion(this);
                SQLiteDatabase db = conexion.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(Usuario.CAMPO_ID, id);
                values.put(Usuario.CAMPO_NOMBRE, nombre);
                values.put(Usuario.CAMPO_TELEFONO, telefono);

                db.insert(Usuario.TABLA_USUARIOS, Usuario.CAMPO_ID, values);
                db.close();

                Toast.makeText(this, "Se registró el usuario correctamente", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);

            } else {
                Toast.makeText(this, "id y nombre son obligatorios", Toast.LENGTH_SHORT).show();
            }
        }

    }
}